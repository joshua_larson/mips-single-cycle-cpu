# README #

MIPS Single Cycle CPU implemented in JLS Logic Designer.  

More info on JLS here: http://www.cs.mtu.edu/~pop/jlsp/bin/JLS.html

Images of CPU logic

Entire CPU:
![CPU.jpg](https://bitbucket.org/repo/nok9AG/images/3767899326-CPU.jpg)

Control Unit:
![control.jpg](https://bitbucket.org/repo/nok9AG/images/3937298030-control.jpg)